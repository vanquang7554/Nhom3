using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using VJPBase.API.Authorization;
using VJPBase.Data.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;
using System.Collections.Generic;
using VJPBase.Data;
using System.Linq;

namespace VJPBase.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChuoiCHRequest : ControllerBase
    {


        private readonly SieuThiContext _context;
        public ChuoiCHRequest(SieuThiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ChuoiCH = _context.ChuoiCHs.ToList();
            return Ok(ChuoiCH);
        }

        [HttpGet("{ID}")]
        public IActionResult GetById(int id)
        {
            var chuoich = _context.ChuoiCHs.SingleOrDefault(x => x.ID == id);
            if (chuoich != null)
            {
                return Ok(chuoich);
            }
            else
            {
                return NotFound();
            }
        }

    }
}
