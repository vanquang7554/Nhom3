using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using VJPBase.API.Authorization;
using VJPBase.Data.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;
using System.Collections.Generic;
using VJPBase.Data;
using System.Linq;

namespace VJPBase.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiSPController : ControllerBase
    {


        private readonly SieuThiContext _context;


        public LoaiSPController(SieuThiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var LoaiSP = _context.LoaiSPs.ToList();
            return Ok(LoaiSP);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var LoaiSP = _context.LoaiSPs.SingleOrDefault(x => x.Id == id);
            if (LoaiSP != null)
            {
                return Ok(LoaiSP);
            }
            else
            {
                return NotFound();
            }

        }


        [HttpPost]
        public IActionResult CreateCustomer(LoaiSPRequest loaiSP)
        {
            var loaisp = new LoaiSP
            {
                NameLSP = loaiSP.Name,
            };
            _context.Add(loaisp);
            _context.SaveChanges();
            return Ok(loaisp);

        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, LoaiSPRequest LoaiSP)
        {
            var loaisp = _context.LoaiSPs.SingleOrDefault(x => x.Id == id);
            if (loaisp != null)
            {
                loaisp.NameLSP = LoaiSP.Name;
                _context.SaveChanges();
                return Ok(loaisp);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
