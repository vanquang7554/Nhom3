﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using VJPBase.API.Authorization;
using VJPBase.Data.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;
using System.Collections.Generic;
using VJPBase.Data;
using System.Linq;

namespace VJPBase.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KhachHangController : ControllerBase
    {


        private readonly SieuThiContext _context;


        public KhachHangController(SieuThiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var khachhang = _context.KhachHangs.ToList();
            return Ok(khachhang);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var khachhang = _context.KhachHangs.SingleOrDefault(x => x.Id == id);
            if (khachhang != null)
            {
                return Ok(khachhang);
            }
            else
            {
                return NotFound();
            }

        }


        [HttpPost]
        public IActionResult CreateCustomer(KhachHangRequest khachHang)
        {
            var khachhang = new KhachHang
            {
                Name = khachHang.Name,
                CCCD = khachHang.CCCD,
                SDT = khachHang.SDT,
                NgaySinh = khachHang.NgaySinh
            };
            _context.Add(khachHang);
            _context.SaveChanges();
            return Ok(khachHang);

        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, KhachHangRequest khachHang)
        {
            var khachhang = _context.KhachHangs.SingleOrDefault(x => x.Id == id);
            if (khachhang != null)
            {
                khachhang.Name = khachHang.Name;
                khachHang.CCCD = khachHang.CCCD;
                khachHang.SDT = khachHang.SDT;
                khachHang.NgaySinh = khachHang.NgaySinh;
                _context.SaveChanges();
                return Ok(khachHang);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
