using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using VJPBase.API.Authorization;
using VJPBase.Data.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;
using System.Collections.Generic;
using VJPBase.Data;
using System.Linq;

namespace VJPBase.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhanVienController : ControllerBase
    {


        private readonly SieuThiContext _context;


        public NhanVienController(SieuThiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var LoaiSP = _context.NhanViens.ToList();
            return Ok(LoaiSP);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var nhanvien = _context.NhanViens.SingleOrDefault(x => x.Id == id);
            if (nhanvien != null)
            {
                return Ok(nhanvien);
            }
            else
            {
                return NotFound();
            }

        }


        [HttpPost]
        public IActionResult CreateCustomer(NhanVienRequest NhanVien)
        {
            var nhanvien = new NhanVien
            {
                Name = NhanVien.Name,
                CCCD = NhanVien.CCCD,
                ChucVu = NhanVien.ChucVu,
                SDT = NhanVien.SDT,
                NgaySinh = NhanVien.NgaySinh,
                ChuoiCHId = NhanVien.ChuoiCHId
            };
            _context.Add(nhanvien);
            _context.SaveChanges();
            return Ok(nhanvien);

        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, NhanVienRequest NhanVien)
        {
            var nhanvien = _context.NhanViens.SingleOrDefault(x => x.Id == id);
            if (nhanvien != null)
            {
                nhanvien.Name = NhanVien.Name;
                nhanvien.CCCD = NhanVien.CCCD;
                nhanvien.ChucVu = NhanVien.ChucVu;
                nhanvien.SDT = NhanVien.SDT;
                nhanvien.NgaySinh = NhanVien.NgaySinh;
                nhanvien.ChuoiCHId = NhanVien.ChuoiCHId;
                _context.SaveChanges();
                return Ok(nhanvien);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
