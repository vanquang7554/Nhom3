﻿using System.Collections.Generic;
using VJPBase.API.Models.Requests;
using VJPBase.API.Models.Response;
using VJPBase.Data.Entities;

namespace VJPBase.API.Services
{
    public interface IKhachHangService
    {
        KhachHangResponse Authenticate(KhachHangRequest model, string ipAddress);
        KhachHangResponse RefreshToken(string token, string ipAddress);
        void RevokeToken(string token, string ipAddress);
        IEnumerable<KhachHang> GetAll();
        KhachHang GetById(int id);

    }
}
