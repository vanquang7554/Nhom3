﻿using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using VJPBase.API.Helpers;
using VJPBase.API.Authorization;
using VJPBase.API.Models.Requests;
using VJPBase.API.Models.Response;
using VJPBase.Data.Entities;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using VJPBase.Data;


namespace VJPBase.API.Services.Impl
{
    public class KhachHangService: IKhachHangService
    {
        private SieuThiContext _context;
        private IJwtUtils _jwtUtils;
        private readonly AppSettings _appSettings;
        private readonly ILogger<UserService> _logger;
        private IHttpContextAccessor _httpContextAccessor;

        public KhachHangService(
            SieuThiContext context,
            IJwtUtils jwtUtils,
            IOptions<AppSettings> appSettings,
            ILogger<UserService> logger,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _jwtUtils = jwtUtils;
            _appSettings = appSettings.Value;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        

        

        KhachHangResponse IKhachHangService.Authenticate(KhachHangRequest model, string ipAddress)
        {
            throw new NotImplementedException();
        }

        KhachHangResponse IKhachHangService.RefreshToken(string token, string ipAddress)
        {
            throw new NotImplementedException();
        }

        void IKhachHangService.RevokeToken(string token, string ipAddress)
        {
            throw new NotImplementedException();
        }

        IEnumerable<KhachHang> IKhachHangService.GetAll()
        {
            return _context.KhachHangs;
        }

        KhachHang IKhachHangService.GetById(int id)
        {
            _logger.LogInformation("get by id");
            var khachHang = _context.KhachHangs.Find(id);
            if (khachHang == null) throw new KeyNotFoundException("User not found");
            return khachHang;
        }
    }
}
