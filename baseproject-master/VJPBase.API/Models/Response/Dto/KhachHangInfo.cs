﻿using System;

namespace VJPBase.API.Models.Response.Dto
{
    public class KhachHangInfo
    {
        public string Name { get; set; }


        public string CCCD { get; set; }
        public string SDT { get; set; }
        public DateTime NgaySinh { get; set; }
    }
}
