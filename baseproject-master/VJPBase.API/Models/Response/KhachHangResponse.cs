﻿using System;
using System.Text.Json.Serialization;
using VJPBase.API.Models.Response.Dto;
using VJPBase.Data.Entities;

namespace VJPBase.API.Models.Response
{
    public class KhachHangResponse
    {
        public KhachHangInfo KhachHangInfo { get; set; }
        public string JwtToken { get; set; }


        [JsonIgnore] // refresh token is returned in http only cookie
        public string RefreshToken { get; set; }

        public KhachHangResponse(KhachHang khach, string jwtToken, string refreshToken)
        {

            KhachHangInfo = new KhachHangInfo
            {

                Name = khach.Name,
                CCCD = khach.CCCD,
                SDT = khach.SDT,
                NgaySinh = khach.NgaySinh
            };
            JwtToken = jwtToken;
            RefreshToken = refreshToken;
        }
    }
}
