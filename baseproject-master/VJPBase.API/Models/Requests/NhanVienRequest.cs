using System;
using System.ComponentModel.DataAnnotations;

namespace VJPBase.API.Models.Requests
{
    public class NhanVienRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string ChucVu { get; set; }

        [Required]
        public string CCCD { get; set; }

        [Required]
        public string SDT { get; set; }

        [Required]
        public DateTime NgaySinh { get; set; }
        [Required]
        public int ChuoiCHId { get; set; }
    }
}
