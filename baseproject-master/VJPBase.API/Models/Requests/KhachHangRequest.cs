﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VJPBase.API.Models.Requests
{
    public class KhachHangRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string CCCD { get; set; }
        [Required]
        public string SDT { get; set; }
        [Required]
        public DateTime NgaySinh { get; set; }
    }
}
