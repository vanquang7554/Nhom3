using System;
using System.ComponentModel.DataAnnotations;

namespace VJPBase.API.Models.Requests
{
    public class LoaiSPRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
