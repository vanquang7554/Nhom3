using System;
using System.ComponentModel.DataAnnotations;

namespace VJPBase.API.Models.Requests
{
    public class ChuoiCHRequest
    {
        [Required]
        public string NameCuaHang { get; set; }

        [Required]
        public string DiaChi { get; set; }
    }
}