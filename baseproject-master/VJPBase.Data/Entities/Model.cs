﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class Model
    {
        public List<ChuoiCH> ChuoiCH { get; set; }
        public List<HoaDon> HoaDon { get; set; }
        public List<KhachHang> KhachHang { get; set; }
        public List<LoaiSP> LoaiSP { get; set; }
        public List<NhanVien> NhanVien { get; set; }
        public List<SanPham> SanPham { get; set; }
        public List<User> User { get; set; }
        public List<CTHD> CTHD { get; set; }
        public Model()
        {
            ChuoiCH = new List<ChuoiCH>();
            HoaDon = new List<HoaDon>();
            KhachHang = new List<KhachHang>();
            LoaiSP = new List<LoaiSP>();
            NhanVien = new List<NhanVien>();
            SanPham = new List<SanPham>();
            User = new List<User>();
            CTHD = new List<CTHD>();
        }
    }
}
