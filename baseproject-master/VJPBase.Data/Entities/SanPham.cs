﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class SanPham
    {
        public int Id { get; set; }


        [Required]
        [MaxLength(10)]
        public string IDLoaiSP { get; set; }
        [ForeignKey("IDLoaiSP")]


        [Required]
        [MaxLength(50)]
        public string NCC { get; set; }

        public LoaiSP LoaiSP { get; set; }
    }
}
