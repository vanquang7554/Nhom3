﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class KhachHang
    {
        public int Id { get; set; }


        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(12)]
        public string CCCD { get; set; }

        [MaxLength(10)]
        public string SDT { get; set; }


        public DateTime NgaySinh { get; set; }
    }
}
