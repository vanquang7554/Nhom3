﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class ChuoiCH
    {
        public int ID { get; set; }


        [Required]
        [MaxLength(50)]
        public string NameCuaHang { get; set; }


        [Required]
        [MaxLength(50)]
        public string DiaChi { get; set; }
    }
}
