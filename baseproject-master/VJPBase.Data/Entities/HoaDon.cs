﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class HoaDon
    {
        public int ID { get; set; }

        [ForeignKey("KhachHangID")]
        [Required]
        public int KhachHangID { get; set; }

        [ForeignKey("NhanVienID")]
        [Required]
        public int NhanVienID { get; set; }

        public DateTime Ngay { get; set; }

        public KhachHang KhachHang { get; set; }

        public NhanVien NhanVien { get; set; }
    }
}
