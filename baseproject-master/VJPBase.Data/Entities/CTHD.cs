﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class CTHD
    {
        [ForeignKey("HoaDonID")]
        [Required]
        public int HoaDonID { get; set; }


        [ForeignKey("SanPhamID")]
        [Required]
        public int SanPhamID { get; set; }

        public float GiaTien { get; set; }
        public int SoLuong { get; set; }

        public HoaDon HoaDon { get; set; }
        public SanPham SanPham { get; set; }
    }
}
