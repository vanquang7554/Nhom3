﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VJPBase.Data.Entities
{
    public class LoaiSP
    {
        public int Id { get; set; }


        [Required]
        [MaxLength(50)]
        public string NameLSP { get; set; }
    }
}
