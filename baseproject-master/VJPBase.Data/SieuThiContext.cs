﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VJPBase.Data.Entities;

namespace VJPBase.Data
{
    public class SieuThiContext : DbContext
    {
        public DbSet<ChuoiCH> ChuoiCHs { get; set; }
        public DbSet<HoaDon> HoaDons { get; set; }
        public DbSet<KhachHang> KhachHangs { get; set; }
        public DbSet<LoaiSP> LoaiSPs { get; set; }
        public DbSet<NhanVien> NhanViens { get; set; }
        public DbSet<SanPham> SanPhams { get; set; }
        public DbSet<CTHD> CTHDs { get; set; }
        public DbSet<User> Users { get; set; }
        public SieuThiContext()
        {
        }

        public SieuThiContext(DbContextOptions<SieuThiContext> options)
               : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=LAPTOP-JG4U1FE7;Database=SieuThiDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CTHD>().HasKey(t => new { t.HoaDonID, t.SanPhamID });

        }
    }
}
